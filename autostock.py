import ctypes
from datetime import datetime 
import pandas as pd
import win32com.client
import time
import sys
import pymysql
import credentials

cpOhlc = win32com.client.Dispatch('CpSysDib.StockChart') # 일자별 ohlc를 얻기 위한 클래스
cpStock = win32com.client.Dispatch('DsCbo1.StockMst') # 현재 종목 가격을 얻기 위한 클래스(현재가, 매수호가, 매도호가) 
cpTradeUtil = win32com.client.Dispatch('CpTrade.CpTdUtil') # 주문 관련 클래스
cpBalance = win32com.client.Dispatch('CpTrade.CpTd6033') # 계좌 정보 클래스
cpCodeMgr = win32com.client.Dispatch('CpUtil.CpStockCode') # 종목 코드 관련 클래스
cpCash = win32com.client.Dispatch('CpTrade.CpTdNew5331A') # 주문 가능 금액 관련 클래스 
cpOrder = win32com.client.Dispatch('CpTrade.CpTd0311') # 실제로 주문하는 기능을 하는 클래스
cpStatus = win32com.client.Dispatch('CpUtil.CpCybos') # 크레온 플러스가 정상 작동하는지 판별하기 위한 클래스

def check_creon_system():
    """크레온 플러스 시스템 연결 상태를 점검한다."""
    # 관리자 권한으로 프로세스 실행 여부
    if not ctypes.windll.shell32.IsUserAnAdmin():
        print('check_creon_system() : admin user -> FAILED')
        return False
 
    # 연결 여부 체크
    if (cpStatus.IsConnect == 0):
        print('check_creon_system() : connect to server -> FAILED')
        return False
 
    # 주문 관련 초기화 - 계좌 관련 코드가 있을 때만 사용
    if (cpTradeUtil.TradeInit(0) != 0):
        print('check_creon_system() : init trade -> FAILED')
        return False
    return True

def get_ohlc(code, qty):
    """인자로 받은 종목의 OHLC 가격 정보를 qty 개수만큼 반환한다."""
    cpOhlc.SetInputValue(0, code)           # 종목코드
    cpOhlc.SetInputValue(1, ord('2'))       # 1:기간, 2:개수으로 받기
    # cpOhlc.SetInputValue(2, toDate)       # To 날짜
    # cpOhlc.SetInputValue(3, fromDate)     # from 날짜
    cpOhlc.SetInputValue(4, qty)             # 요청개수
    cpOhlc.SetInputValue(5, [0, 2, 3, 4, 5]) # 0:날짜, 2~5:OHLC 8:거래량
    cpOhlc.SetInputValue(6, ord('D'))        # D:일단위
    cpOhlc.SetInputValue(9, ord('1'))        # 0:무수정주가, 1:수정주가
    cpOhlc.BlockRequest()

    count = cpOhlc.GetHeaderValue(3)   # 3:수신개수

    columns = ['open', 'high', 'low', 'close']

    index = []
    rows = []

    for i in range(count): 
        index.append(cpOhlc.GetDataValue(0, i)) 
        rows.append([cpOhlc.GetDataValue(1, i), cpOhlc.GetDataValue(2, i),
            cpOhlc.GetDataValue(3, i), cpOhlc.GetDataValue(4, i)]) 

        # 0:일자, 1:시가, 2:고가, 3:저가, 4:종가
    df = pd.DataFrame(rows, columns=columns, index=index)
    return df  

def get_target_price(code): 
    """래리 윌리엄스의 k = 0.5 변동성 돌파 전략에 target price를 리턴한다"""

    try:
        time_now = datetime.now()
        str_today = time_now.strftime('%Y%m%d')
        ohlc = get_ohlc(code, 10)

        if str_today == str(ohlc.iloc[0].name) :
            lastday = ohlc.iloc[1]
            today_open = ohlc.iloc[0].open
            
        else :
            lastday = ohlc.iloc[0]
            today_open = lastday.close

        lastday_high = lastday.high
        lastday_low = lastday.low

        target_price = today_open + (lastday_high - lastday_low) * 0.5 

        return target_price 
    
    except Exception as ex:
        print("get_target_price() -> exception! " + str(ex))
        return None

def get_moving_average(code, window):
    """ 해당 윈도우의 이동 평균을 리턴한다 """
    try : 
        time_now = datetime.now()
        str_today = time_now.strftime('%Y%m%d')
        ohlc = get_ohlc(code, 40)

        if str_today == str(ohlc.iloc[0].name):
            lastday = ohlc.iloc[1].name
        else:
            lastday = ohlc.iloc[0].name
        closes = ohlc['close'].sort_index()
        average = closes.rolling(window=window).mean()
        return average.loc[lastday]

    except Exception as ex:
        print('get_moving_average() -> exception! ' + str(ex))
        return None

def get_current_price_info(code):
    """ 종목의 현재가, 매도호가, 매수호가를 리턴한다"""
    cpStock.SetInputValue(0, code) #종목 코드에 대한 가격 정보
    cpStock.BlockRequest()

    item = {}
    item['current_price'] = cpStock.GetHeaderValue(11) # 현재가 
    item['offer'] = cpStock.GetHeaderValue(16) # 매도호가 => 해당 종목을 팔려고 내놓은 최저 가격
    item['bid'] = cpStock.GetHeaderValue(17) # 매수호가 => 해당 종목을 사려고 하는 최고 가격
    # code = cpStock.GetHeaderValue(0) 
    # name = cpStock.GetHeaderValue(1) 
    # time = cpStock.GetHeaderValue(4) 
    # diff = cpStock.GetHeaderValue(12)
    # open = cpStock.GetHeaderValue(13)
    # high = cpStock.GetHeaderValue(14) 
    # low = cpStock.GetHeadderValue(15) 
    # volumn = cpStock.GetHeaderValue(17) # 거래량
    # volumn_value = cpStock.GetHeaderValue(19) #거래대금 

    return item

def get_stock_balance(code = 'ALL') :
    """ 종목 코드를 받으면 해당 종목의 종목명과 수량을 반환,
        'ALL'을 받으면 현재 계좌에 보유 중인 모든 종목에 대해서 종목명과 수량 반환
        """
    cpTradeUtil.TradeInit()
    acc = cpTradeUtil.AccountNumber[0] #계좌 번호
    accFlag = cpTradeUtil.GoodsList(acc, 1) # -1: 전체, 1: 주식, 2: 선물/옵션
    cpBalance.SetInputValue(0, acc) # 계좌번호 
    cpBalance.SetInputValue(1, accFlag[0]) # 상품 구분 - 주식 상품 중 첫 번째
    cpBalance.SetInputValue(2, 50) # 요청 건수 최대 50
    ret = cpBalance.BlockRequest()

    if ret == 4 :
        remain_time = cpStatus.LimitRequestRemainTime
        print("대기시간 : ", remain_time / 1000)
        time.sleep(remain_time / 1000 + 1)
        ret = cpBalance.BlockRequest()

    if code == "ALL" :
        print('계좌 번호 : ', acc)
        print('계좌명 : ' + str(cpBalance.GetHeaderValue(0)))
        print('결제잔고수량 : ' + str(cpBalance.GetHeaderValue(1)))
        print('평가금액 : ' + str(cpBalance.GetHeaderValue(3)))
        print('평가손익 : ' + str(cpBalance.GetHeaderValue(4)))
        print('종목수 : ' + str(cpBalance.GetHeaderValue(7)))

    stocks = []

    number_of_stocks = cpBalance.GetHeaderValue(7)

    for i in range(number_of_stocks):
        stock_code = cpBalance.GetDataValue(12, i) # 개별 종목코드
        stock_name = cpBalance.GetDataValue(0, i) # 개별 종목명
        stock_qty = cpBalance.GetDataValue(15, i) # 수량
        stock_price = cpBalance.GetDataValue(17, i) # 체결 장부 단가, 매수 가격의 평균
        stock_purchase_price = stock_qty *  stock_price # 매입가 
        stock_evaluated_price = cpBalance.GetDataValue(9, i) # 평가금액 (천원 미만은 절사)
        stock_profit_and_loss = cpBalance.GetDataValue(11, i) # 평가손익 (천원 미만은 절사)

        if code == 'ALL':
            stocks.append({
                'code' : stock_code, 
                'name' : stock_name, 
                'qty' : stock_qty, 
                'price' : stock_price, 
                'purchase_price' : stock_purchase_price, 
                'evaluated_price' : stock_evaluated_price, 
                'profit_and_loss' : stock_profit_and_loss,
            })

        if stock_code == code:
            return {
                'code' : code,
                'name' : stock_name,
                'qty' : stock_qty,
                'purchase_unit_price' : stock_price,                
            }

    if code == 'ALL':
        return {
            'stocks' : stocks
        }
    else:
        stock_name = cpCodeMgr.CodeToName(code)
        return {
            'code' : code,
            'name' : stock_name,
            'qty' : 0,
            'purchase_unit_price' : 0,
        }
        
def get_current_cash() :
    """ 현재 매수 가능 금액 (현금이 얼마나 있는지) 리턴 """
    cpTradeUtil.TradeInit()
    acc = cpTradeUtil.AccountNumber[0] # 계좌 번호
    
    accFlag = cpTradeUtil.GoodsList(acc, 1) # -1 전체, 1주식, 2: 선물/옵션
    cpCash.SetInputValue(0, acc)
    cpCash.SetInputValue(1, accFlag[0])
    cpCash.SetInputValue(2, 'A005930')
    ret = cpCash.BlockRequest()
    current_cash = cpCash.GetHeaderValue(9)
    
    return current_cash
    
def buy_stocks(code) :
    """ 해당 종목 코드의 주식을 사는 기능 """
    try:
        global bought_list
        for item in bought_list :
            if code in item['code'] :
                return

        time_now = datetime.now()
        item_to_buy = get_current_price_info(code)
        target_price = get_target_price(code)
        m5_price = get_moving_average(code, 5)
        m10_price = get_moving_average(code, 10)
        buy_qty = 0 # 매수할 수량 초기화

        if item_to_buy['offer'] > 0 : # 매도호가가 존재하면
            buy_qty = buy_amount // item_to_buy['offer'] 
            already_bought_item = get_stock_balance(code)

            if item_to_buy['current_price'] < target_price or item_to_buy['current_price'] < m5_price \
                 or item_to_buy['current_price'] < m10_price :
                 return  

            cpTradeUtil.TradeInit()
            acc = cpTradeUtil.AccountNumber[0]
            accFlag = cpTradeUtil.GoodsList(acc, 1)
            cpOrder.SetInputValue(0, '2') # 1: 매도, 2: 매수
            cpOrder.SetInputValue(1, acc) # 계좌번호 설정
            cpOrder.SetInputValue(2, accFlag[0]) # 상품 구분
            cpOrder.SetInputValue(3, code) # 종목 코드
            cpOrder.SetInputValue(4, buy_qty) # 매수 수량
            cpOrder.SetInputValue(8, "03") # 5 조건부, 12: 최유리, 13 최우선 

            ret = cpOrder.BlockRequest() 
            if ret == 4 :
                remain_time = cpStatus.LimitRequestRemainTime
                print("!!! 연속 주문에 제한이 걸렸습니다!!! -> 대기시간 : ", remain_time / 1000)
                time.sleep(remain_time / 1000)
                return False

            time.sleep(2)
            item_of_bought = get_stock_balance(code)
            if ret == 0 :
                bought_list.append({"code" : code, "qty" : item_of_bought['qty']})
                write_account_balance()
                update_stock_balance(item_of_bought)
            
    except Exception as ex : 
        print("buy_stocks() exception error" + str(ex))

def sell_all() :
    """ 보유한 주식 모두 판매 """
    try :
        global bought_list
        cpTradeUtil.TradeInit()
        acc = cpTradeUtil.AccountNumber[0] # 계좌번호
        accFlag = cpTradeUtil.GoodsList(acc, 1) 

        while True :
            balance = get_stock_balance('ALL')
            total_qty = 0
            for stock in balance['stocks'] :
                total_qty += stock['qty']

            if total_qty == 0 :
                return True

            for stock in balance['stocks'] :
                cpOrder.SetInputValue(0, "1") # 매도
                cpOrder.SetInputValue(1, acc) # 계좌번호
                cpOrder.SetInputValue(2, accFlag[0]) # 주식 상품중 첫 번째
                cpOrder.SetInputValue(3, stock['code']) # 종목코드
                cpOrder.SetInputValue(4, stock['qty']) # 매도수량
                cpOrder.SetInputValue(8, "03") 

                ret = cpOrder.BlockRequest()
                item_after_sell = get_stock_balance(stock['code']) 

                if ret == 0 and item_after_sell['qty'] == 0 :
                    delete_stock_balance(stock['code'])
                    write_account_balance()
                elif ret == 0 and item_after_sell['qty'] != 0 :
                    update_stock_balance(item_after_sell)
                    write_account_balance()
                print("시장가 일반 매도", stock['code'], stock['name']," 매도수량: ", stock['qty'])

                time.sleep(1)
            time.sleep(10)

    except Exception as ex :
        print("sell_all() -> exception! " + str(ex))

def delete_stock_balance(code) :
    try : 
        curs = conn.cursor()
        user_id = 1
        code = code[1:]
        sql = "DELETE from autostock_stockbalance where user_id=%s and company_code=%s"
        curs.execute(sql, (user_id, code))
        conn.commit()
    except : 
        pass
        
def update_stock_balance(item) :

    print(item)
    curs = conn.cursor()
    user_id = 1
    code = item['code'][1:]

    sql = "SELECT * from autostock_stockbalance where user_id = %s and company_code = %s"
    curs.execute(sql, (user_id, code))
    rs = curs.fetchone()

    if rs :
        sql = "UPDATE autostock_stockbalance set holding_quantity = %s, purchase_unit_price = %s where user_id = %s and company_code = %s"
        curs.execute(sql, (item['qty'], item['purchase_unit_price'], user_id, code))
        conn.commit()
    
    else :
        write_stock_balance(item)

def write_stock_balance(item) :
    curs = conn.cursor()
    code = item['code'][1:] # delete 'A'
    user_id = 1
    sql = """INSERT INTO autostock_stockbalance(user_id, company_code, holding_quantity, purchase_unit_price) 
         values (%s, %s, %s, %s)"""
    curs.execute(sql, (user_id, code, item['qty'], item['purchase_unit_price']))
    conn.commit()

def write_account_balance() :
    curs = conn.cursor()
    cpTradeUtil.TradeInit()
    acc = cpTradeUtil.AccountNumber[0]
    accFlag = cpTradeUtil.GoodsList(acc, 1)
    
    cpBalance.SetInputValue(0, acc) # 계좌번호 
    cpBalance.SetInputValue(1, accFlag[0]) # 상품 구분 - 주식 상품 중 첫 번째
    cpBalance.SetInputValue(2, 50) # 요청 건수 최대 50

    ret = cpBalance.BlockRequest()
    
    total_evaluation_amount = cpBalance.GetHeaderValue(3)
    total_valuation_profit_or_loss = cpBalance.GetHeaderValue(4)
    rate_of_return = cpBalance.GetHeaderValue(8)
    user_id = 1

    sql = f"REPLACE INTO autostock_realtimeaccountbalance\
        (user_id, account_number, total_evaluation_amount, total_valuation_profit_or_loss, rate_of_return)\
        VALUES ({ user_id }, '{ acc }', '{ total_evaluation_amount }', '{ total_valuation_profit_or_loss }', '{ rate_of_return }');"
    curs.execute(sql)
    conn.commit() 
    
if __name__ == '__main__': 
    try:
        conn = pymysql.connect(
            host = credentials.maria_db['host'], 
            user = credentials.maria_db['user'],
            password = credentials.maria_db['password'], 
            db = credentials.maria_db['db'], 
            charset='utf8'
        )
        symbol_list = []

        sql = """ 
        SELECT * 
        FROM autostock_stocksofinterest 
        WHERE user_id = 1
        """
        df = pd.read_sql(sql, conn)
        
        for idx in range(len(df)):
            symbol_list.append('A' + df['company_code'].values[idx])
       
        print("타겟 리스트(관심 종목 리스트): ", symbol_list)
        bought_list = []     # 매수한 종목 리스트
        target_buy_count = 5 # 매수할 종목 수
        buy_percent = 0.19   

        print('check_creon_system() :', check_creon_system())  # 크레온 접속 점검
        total_cash = int(get_current_cash())   # 100% 증거금 주문 가능 금액 조회
        buy_amount = total_cash * buy_percent  # 종목별 주문 금액 계산

        print('시작 시간 :', datetime.now().strftime('%m/%d %H:%M:%S'))
        soldout = False;
        sell_all()
        
        while True:
            t_now = datetime.now()
            t_9 = t_now.replace(hour=9, minute=0, second=0, microsecond=0)
            t_start = t_now.replace(hour=9, minute=5, second=0, microsecond=0)
            t_sell = t_now.replace(hour=15, minute=15, second=0, microsecond=0)
            t_exit = t_now.replace(hour=15, minute=20, second=0,microsecond=0)
            today = datetime.today().weekday()
            if today == 5 or today == 6:  # 토요일이나 일요일이면 자동 종료
                print('Today is', 'Saturday.' if today == 5 else 'Sunday.')
                sys.exit(0)

            # 장이 시작하고 어제 팔지 못한 종목들을 판매한다.
            if t_9 < t_now < t_start and soldout == False:
                soldout = True
        
            if t_start < t_now < t_sell :  # AM 09:05 ~ PM 03:15 : 매수
                # 미체결 결제될 경우 update
                print('구매 리스트[', end=' ')
                for item in bought_list :
                    if item['qty'] > 0 :
                        print('종목명: {', cpCodeMgr.CodeToName(item['code']), '수량:', item['qty'], '}', end=' ')
                print(']')
                for idx, bought_item in enumerate(bought_list):
                    item = get_stock_balance(bought_item['code'])
                    if item['qty'] != bought_item['qty'] :
                        if item['qty'] == 0 :
                            delete_stock_balance(item)
                        else :
                            update_stock_balance(item)
                        write_account_balance()
                        bought_list[idx]['qty'] = item['qty']    
                    time.sleep(1)
                for sym in symbol_list:
                    if len(bought_list) < target_buy_count:
                        buy_stocks(sym)
                        time.sleep(1)
                if t_now.minute == 30 and 0 <= t_now.second <= 5: 
                    get_stock_balance('ALL')
                    time.sleep(5)

            if t_sell < t_now < t_exit:  # PM 03:15 ~ PM 03:20 : 일괄 매도
                if sell_all() == True:
                    sys.exit(0)
            if t_exit < t_now:  # PM 03:20 ~ :프로그램 종료
                sys.exit(0)

            time.sleep(10)
    except Exception as ex:
        print('`main -> exception! ' + str(ex) + '`')


